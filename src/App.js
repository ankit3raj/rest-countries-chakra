import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CountryCard from "./component/country";
import CountryDetails from "./component/countryDetail";
import "./App.css";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/">
            <CountryCard />
          </Route>

          <Route path="/code/:code" component={CountryDetails}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
