import React, { Component } from "react";
import * as CountryApi from "./fetchingApi";
import ClockLoader from "react-spinners/ClockLoader";
import { Link } from "react-router-dom";
import {
  Box,
  Image,
  Badge,
  Text,
  VStack,
  Input,
  Select,
  Heading,
  Button,
  Flex,
  Spacer,
} from "@chakra-ui/react";

class CountryCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
      isLoading: true,
      hasError: false,
      searchTerm: [],
      regionSelect: "All",
      regions: [],
      ColorMode: "light",
    };
  }
  componentDidMount() {
    CountryApi.getCountries()
      .then((res) => {
        this.setState({
          countries: res,
          isLoading: false,
          hasError: false,
          ColorMode: null,
        });
      })
      .catch((err) => {
        this.setState({
          err,
          isLoading: false,
          hasError: true,
        });
      });
  }

  render() {
    const filteredCountries = this.state.countries.filter(
      (c) =>
        c.name.common.toLowerCase().includes(this.state.searchTerm) &&
        (this.state.regionSelect === "All" ||
          c.region === this.state.regionSelect)
    );
    if (this.state.isLoading) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <ClockLoader color="#36d7b7" />
        </Flex>
      );
    }
    if (this.state.hasError) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Text color="#36d7b7">Internal Server Error</Text>
        </Flex>
      );
    }

    const regions = [
      ...this.state.countries.reduce((acc, curr) => {
        acc.add(curr.region);
        return acc;
      }, new Set()),
    ];
    return (
      <>
        {/* <Container> */}
        <Flex bg="white" h={20}>
          <Heading as="h1" fontWeight="bold" ml={10} mt={4}>
            Where in the world?
          </Heading>
          <Spacer />
          <Button onClick={this.state.ColorMode} mr={10} mt={4}>
            Mode {this.state.ColorMode === "light" ? "Dark" : "Light"}
          </Button>
        </Flex>
        {/* </Container> */}
        {/* <Container> */}
        <Flex justify="space-between" bg="rgb(243, 240, 240)">
          <Input
            placeholder="Search"
            type="text"
            size="sm"
            w="sm"
            m={10}
            p={6}
            border="1px solid white"
            value={this.state.searchTerm}
            onChange={(e) => this.setState({ searchTerm: e.target.value })}
          />
          <Select
            variant="outline"
            placeholder="Type"
            size="sm"
            width="200px"
            m={10}
            mr={14}
            border="1px solid white"
            // p={6}
            value={this.state.regionSelect}
            onChange={(e) => this.setState({ regionSelect: e.target.value })}
          >
            <option value="All">All</option>
            {regions.map((el) => (
              <option key={el}>{el}</option>
            ))}
          </Select>
        </Flex>
        <Flex wrap="wrap" bg="rgb(243, 240, 240)">
          {filteredCountries.length === 0 && (
            <Flex
              bg="rgb(243, 240, 240)"
              justify="center"
              align="center"
              h="80vh"
              w="100vw"
            >
              <Text>No Country starts with this word</Text>{" "}
            </Flex>
          )}
          {filteredCountries.map((item) => {
            return (
              //   <HStack>
              <Box
                w="300px"
                rounded="20px"
                overflow="hidden"
                bg={this.state.ColorMode === "dark" ? "gray.700" : "gray.200"}
                m={20}
              >
                <Image
                  src={item.flags.png}
                  alt="Country Flag"
                  boxSize="300px"
                ></Image>
                <Box p={5}>
                  <VStack align="center">
                    <Badge
                      variant="solid"
                      colorScheme="green"
                      rounded="full"
                      px={2}
                    >
                      <Link to={"/code/" + item.cca2}>
                        <Text>{item.name.common}</Text>
                      </Link>
                      {/* {item.name.common} */}
                    </Badge>
                  </VStack>
                  <VStack align="center">
                    <Text as="h2" fontWeight="bold" my={2}>
                      Region: {item.region}
                    </Text>
                    <Text fontWeight="bold">Population: {item.population}</Text>
                    <Text fontWeight="bold">Capital: {item.capital}</Text>
                  </VStack>
                  <Flex>
                    <Spacer />
                  </Flex>
                </Box>
              </Box>
              //   </HStack>
            );
          })}
        </Flex>
        {/* </Container> */}
      </>
    );
  }
}

export default CountryCard;
