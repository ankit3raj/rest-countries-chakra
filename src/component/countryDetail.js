import React, { Component } from "react";
import * as CountryApi from "./fetchingApi";
import ClockLoader from "react-spinners/ClockLoader";
import { Link } from "react-router-dom";
import {
  Image,
  Text,
  HStack,
  VStack,
  Heading,
  Button,
  Flex,
  Spacer,
} from "@chakra-ui/react";

class CountryDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: [],
      isLoading: true,
      hasError: false,
    };
  }
  componentDidMount() {
    const { code } = this.props.match.params;

    console.log("calling Api");
    CountryApi.getCountryByName(code)
      .then((res) => {
        this.setState({
          country: res[0],
          isLoading: false,
          hasError: false,
        });
      })
      .catch((err) => {
        this.setState({
          country: null,
          isLoading: false,
          hasError: true,
        });
      });
  }
  async componentDidUpdate(prevProps) {
    if (prevProps.match.params.code !== this.props.match.params.code) {
      try {
        const { code } = this.props.match.params;
        const data = await CountryApi.getCountryByName(code);
        this.setState({
          country: data[0],
          isLoading: false,
          hasError: false,
        });
      } catch (err) {
        this.setState({
          countryy: null,
          isLoading: false,
          hasError: true,
        });
      }
    }
  }

  render() {
    console.log(this.state.country);
    if (this.state.isLoading) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <ClockLoader color="#36d7b7" />
        </Flex>
      );
    }
    if (this.state.hasError) {
      return (
        <Flex>
          <Text color="#36d7b7">Internal Server Error</Text>
        </Flex>
      );
    }
    return (
      <>
        <Flex bg="white" h={20}>
          <Heading as="h1" fontWeight="bold" ml={10} mt={4}>
            Where in the world?
          </Heading>
          <Spacer />
          <Button onClick={this.state.ColorMode} mr={10} mt={4}>
            Mode {this.state.ColorMode === "light" ? "Dark" : "Light"}
          </Button>
        </Flex>
        <Flex justify="space-between" bg="rgb(243, 240, 240)">
          <Button m={10} border="1px solid black">
            <Link to="/">Back</Link>
          </Button>
        </Flex>
        <Flex
          justify="center"
          bg="rgb(243, 240, 240)"
          align="center"
          h="100%"
          //   wrap="wrap"
        >
          <Image
            src={this.state.country.flags.png}
            alt="country"
            w="450px"
            h="350px"
            ml={10}
            mr={10}
          ></Image>
          <HStack>
            <VStack align="left" w="500px">
              <Heading as="h1" fontWeight="900" mb={3}>
                {this.state.country.name.common}
              </Heading>
              <Text as="h3" fontWeight="bold">
                Native Name:{"   "}
                {Object.values(this.state.country.name.nativeName)[0].common}
              </Text>
              <Text as="h3" fontWeight="bold">
                Population:{"  "}
                {this.state.country.population}
              </Text>
              <Text as="h3" fontWeight="bold">
                Region:{"  "}
                {this.state.country.region}
              </Text>
              <Text as="h3" fontWeight="bold">
                Sub-Region:{"  "}
                {this.state.country.subregion}
              </Text>
              <Text as="h3" fontWeight="bold">
                Capital:{"  "}
                {this.state.country.capital}
              </Text>

              {this.state.country.borders &&
              this.state.country.borders.length ? (
                <Text as="h3" fontWeight="bold">
                  Border Countries:
                  {this.state.country.borders.map((el) => {
                    return (
                      <Link key={el} to={`/code/${el}`}>
                        <Button m={0.5}>{el}</Button>
                      </Link>
                    );
                  })}
                </Text>
              ) : null}
            </VStack>
            <VStack align="left">
              <Text as="h3" fontWeight="bold">
                Top Level Domain:{"  "}
                {this.state.country.tld}
              </Text>
              <Text as="h3" fontWeight="bold">
                Currencies:{"  "}
                {Object.values(this.state.country.currencies)[0].name}
              </Text>
              <Text as="h3" fontWeight="bold">
                Language:{"  "}
                {Object.values(this.state.country.languages)}
              </Text>
            </VStack>
          </HStack>
        </Flex>
      </>
    );
  }
}
export default CountryDetails;
