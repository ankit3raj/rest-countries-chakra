import axios from "axios";

export const getCountries = () => {
  return axios
    .get("https://restcountries.com/v3.1/all")
    .then((res) => res.data);
};

export const getCountryByName = (code) => {
  console.log("in APi");
  return axios
    .get(`https://restcountries.com/v3.1/alpha/${code}`)
    .then((res) => res.data);
};
